use crate::lox_context::LoxServerContext;
use hyper::{body, header::HeaderValue, Body, Method, Request, Response, StatusCode};
use std::convert::Infallible;

// Handle requests used in testing
pub async fn handle(
    cloned_context: LoxServerContext,
    req: Request<Body>,
) -> Result<Response<Body>, Infallible> {
    match req.method() {
        &Method::OPTIONS => Ok(Response::builder()
            .header("Access-Control-Allow-Origin", HeaderValue::from_static("*"))
            .header("Access-Control-Allow-Headers", "accept, content-type")
            .header("Access-Control-Allow-Methods", "POST")
            .status(200)
            .body(Body::from("Allow POST"))
            .unwrap()),
        _ => match (req.method(), req.uri().path()) {
            (&Method::POST, "/advancedays") => Ok::<_, Infallible>({
                let bytes = body::to_bytes(req.into_body()).await.unwrap();
                cloned_context.advance_days_with_response_test(bytes)
            }),
            _ => {
                // Return 404 not found response.
                Ok(Response::builder()
                    .status(StatusCode::NOT_FOUND)
                    .body(Body::from("Not found"))
                    .unwrap())
            }
        },
    }
}
